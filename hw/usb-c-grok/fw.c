/*
 * This file is part of the sigrok-firmware-fx2lafw project.
 *
 * Copyright (C) 2018 Peter Hazenberg <sigrok@haas-en-berg.nl>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FX2LAFW_HW_USB_C_GROK
#define FX2LAFW_HW_USB_C_GROK
#endif

#include <../fx2lafw.c>
#include <i2c.h>

BYTE buf[2];

BYTE INA219_I2C_ADDRESS   = 0x40;
BYTE INA219_CONFIGURATION = 0x00;
BYTE INA219_SHUNT_VOLTAGE = 0x01;
BYTE INA219_BUS_VOLTAGE   = 0x02;
BYTE INA219_POWER         = 0x03;
BYTE INA219_CURRENT       = 0x04;
BYTE INA219_CALIBRATION   = 0x05;

// Timer 0 interrupt handler
void isr_TF0() __interrupt(TF0_ISR) {
	TR0 = 0; // Disable Timer 0 counting, to prevent from triggering again while we aren't done yet
	i2c_register_read(INA219_I2C_ADDRESS, INA219_BUS_VOLTAGE, 2, 0);
	i2c_register_read(INA219_I2C_ADDRESS, INA219_CURRENT,     2, 0);
	i2c_register_read(INA219_I2C_ADDRESS, INA219_POWER,       2, 0);
	TH0 = 0; // Reset counter, high byte
	TL0 = 0; // Reset counter, low byte
	TR0 = 1; // Re-enable Timer 0 counting
}

void main(void) {
	TMOD = 0; // Set timer 0 to 13 bit mode
	ET0 = 1; // Enable Timer 0 interrupt
	TR0 = 1; // Enable counting on Timer 0.
	EIP |= PUSB; // Set USB interrupt priority to high

	fx2lafw_init();
	while (1) {
		fx2lafw_poll();

		if(gpif_acquiring == RUNNING && !TR0) {
			// Config register hardcoded for simplicity
			// mode: Shunt and bus voltage, continuous
			// shunt adc: 12 bit
			// bus adc: 12 bit
			// gain: 0.5
			// range: 32
			// reset: False
			buf[0] = 0x29; buf[1] = 0x9f;
			i2c_write(INA219_I2C_ADDRESS, 1, &INA219_CONFIGURATION, 2, buf);

			// Calibration register, current LSB: 121.952 µA
			buf[0] = 0x83; buf[1] = 0x33;
			i2c_write(INA219_I2C_ADDRESS, 1, &INA219_CALIBRATION, 2, buf);

			// start Timer 0 counting *after* setting conf and cal registers
			TR0 = TRUE;

		} else if(gpif_acquiring != RUNNING) {
			TR0 = FALSE;
		}
	}
}
